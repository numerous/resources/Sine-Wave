import logging
import math
from numerous.image_tools.job import NumerousSimulationJob
from numerous.image_tools.app import run_job

logger = logging.getLogger('Sine Wave')


class SineWaveJob(NumerousSimulationJob):
    def __init__(self):
        super(SineWaveJob, self).__init__()

    def initialize_simulation_system(self):
        self.align_outputs_to_next_timestep = False

    def step(self, t: float = None, dt: float = None):
        #logger.warning("We are sine waving") # This will log at every time interval dt
        outputs = {"Sine": math.sin(math.radians(t/10))} # a dict with outputs
        return t + dt, outputs

    def serialize_states(self, t: float = None):
        return True

def run_example():
    run_job(numerous_job=SineWaveJob(), appname="Sine Wave", model_folder=".")

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    run_example()
    